const list = document.getElementById('list')!;
const input = document.getElementById('input')! as HTMLInputElement;
const add = document.getElementById('add')!;
const edit = document.getElementById('edit')!;

// What our data model looks like to store down
interface ListItemModel {
  id: string;
  name: string;
  age: number;
}

// Working list of items that we will add to and remove from
let listOfItems: ListItemModel[] = JSON.parse(
  localStorage.getItem('list') || '[{ "id": "172183", "name": "hello", "age": 4 }]'
);

// Render the list loaded from storage
reRenderList();

// Add a new model to our list when we click the add button
add.addEventListener('click', () => {
  // The data here is arbitrary for now
  addItem({
    id: Math.random()
      .toString()
      .slice(9), // generate some random ID we can use later
    name: input.value,
    age: Math.floor(Math.random() * 60)
  });
});


// Remove a single item from the list by its id, rerender the list and store the updated list
function removeItemById(id: string) {
		listOfItems = listOfItems.filter(item => item.id !== id);
		reRenderList();
	storeListOfItems();
}

// Add a new item to our list of items, rerender the list and store the updated list
function addItem(item: ListItemModel) {
  listOfItems = listOfItems.concat(item);
  reRenderList();
  storeListOfItems();
}

// Edit item on our list of items, rerender the list and store the updated list
function editItemById(id: string, data) {
 const index = listOfItems.findIndex(item => item.id === id);
 listOfItems[index] = data;
 reRenderList();
 storeListOfItems();
}

//let mode = 'read';

enum Mode {
  EDIT,
  READ
};

let mode: Mode = Mode.EDIT;
let textValue = 'My Initial Value';

document.getElementById('toggle').addEventListener('click', () => {
  reRenderList();
  });

function reRenderList() {
const htmlListItems = listOfItems.map(item => {

////////////////////// edit text ///////////////

    const inputText = document.createElement('input');
  if(mode === Mode.EDIT) {
    mode = Mode.READ;
    inputText.readOnly = true;
    inputText.className = 'readOnly';
  } else {
    mode = Mode.EDIT;
    inputText.readOnly = false;
    inputText.className = 'editable';
    inputText.addEventListener('keyup', () => {
      textValue = input.value;
    })
  }
    input.value = textValue;
	inputText.innerHTML = 'TEXT';
	
  //document.getElementById('app').innerHTML = '';
  //document.getElementById('app').appendChild(input);
  
  ////////////////////// edit buttons ///////////////
  
  // Create an edit button and setup click handler for it
  
  let editButton = document.createElement('button');
  let saveButton = document.createElement('button');
  
  if(mode === Mode.READ) {
  //render an edit button
  editButton.addEventListener('click', () =>
  { mode = Mode.EDIT;
  editItemById(item.id, {
    ...item,
    name: input.value}	
  )});
  editButton.innerHTML = 'Edit';
  } else if (mode === Mode.EDIT){
  // Create button and setup click handler for it
  saveButton.addEventListener('click', () =>
  { mode = Mode.READ;
  editItemById(item.id, {
    ...item,
    name: input.value}
  )});
  saveButton.innerHTML = 'Save';
  }
  
  // create remove button to delete saved URL
	const removeButton = document.createElement('button');
	removeButton.addEventListener('click', () => removeItemById(item.id));
	removeButton.innerHTML = 'remove';

  // create the list item to render
  const listItem = document.createElement('li');

  // add the input and save button to it
  
 //if(mode === 'read') {
  //listItem.appendChild(inputText);
  //listItem.appendChild(editButton);
  //} else if (mode === 'editButton'){
  //listItem.appendChild(input);
  //listItem.appendChild(saveButton);}
  listItem.appendChild(inputText);
  listItem.appendChild(removeButton);

  
  return listItem;
  });
  
  const ul = document.getElementById('list');
// clear out the current list
ul.innerHTML = '';
htmlListItems.forEach(item => ul.appendChild(item));

////////////////////// edit button ///////////////

}

// Update local storage every time our list mutates.
function storeListOfItems() {
  localStorage.setItem('list', JSON.stringify(listOfItems));
}

